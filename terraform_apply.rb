def get_google_machine_state(state_module)
    id = state_module['resources']['google_compute_instance.default']['primary']['id']
    public_ip = state_module['resources']['google_compute_instance.default']['primary']['attributes']['network_interface.0.address']
    zone = state_module['resources']['google_compute_instance.default']['primary']['attributes']['zone']
    @util.json("{\"id\" : \"#{id}\", \"public_ip\" : \"#{public_ip}\", \"zone\" : \"#{zone}\"}")
    # comment_body = "Created an instance with following details, Instance ID : #{id} | Zone : #{zone} | Public IP : #{public_ip}"
end

def get_aws_machine_state(state_module)
    id = state_module['resources']['aws_instance.example']['primary']['id']
    availability_zone = state_module['resources']['aws_instance.example']['primary']['attributes']['availability_zone']
    instance_type = state_module['resources']['aws_instance.example']['primary']['attributes']['instance_type']
    private_ip = state_module['resources']['aws_instance.example']['primary']['attributes']['private_ip']
    public_ip = state_module['resources']['aws_instance.example']['primary']['attributes']['public_ip']
    @util.json("{\"id\" : \"#{id}\", \"public_ip\" : \"#{public_ip}\", \"availability_zone\" : \"#{availability_zone}\",\"instance_type\" : \"#{instance_type}\", \"private_ip\" : \"#{private_ip}\"}")
    # comment_body = "Created an instance with following details, Instance ID : #{id} | Availability zone : #{availability_zone} | Instance type : #{@instance_type} | Private IP : #{private_ip} | Public IP : #{public_ip}"
end

@log.trace('Started execution of jira-terraform:terraform_apply.rb flintbit..')
begin
    # Getting required parameters from global config
    @ssh_connector_name = @config.global('terraform.ssh_connector.name')
    @target = @config.global('terraform.ssh_connector.hostname')
    @username = @config.global('terraform.ssh_connector.username')
    @password = @config.global('terraform.ssh_connector.password')
    @terraform_dir = @config.global('terraform.dir')

    # Getting input parameters
    @id = @input.get('id')                         #
    @provider = @input.get('provider')             # Provider name like 'aws','digital ocean'
    @instance_type = @input.get('instance_type')   # Type of instance
    @os = @input.get('os')

    @log.info("ID : #{@id}, Provider : #{@provider}, Instance type : #{@instance_type}, OS : #{@os}")

    # remove whitespaces
    @provider = @provider.delete(' ')
    @os = @os.delete(' ')

    @provider.downcase!
    @instance_type.downcase!
    @os.downcase!

    # Building apply command
    if @provider.include? 'google'
        @apply_command = "cd #{@terraform_dir}/#{@provider}-#{@os}-#{@instance_type};terraform apply -state='#{@id}.json' -var 'name=vm-#{@id}';"
    else
        @apply_command = "cd #{@terraform_dir}/#{@provider}-#{@os}-#{@instance_type};terraform apply -state='#{@id}.json';"
    end

    # Building get state command
    @get_state_command = "cd #{@terraform_dir}/#{@provider}-#{@os}-#{@instance_type};cat #{@id}.json;"

    connector_call = @call.connector(@ssh_connector_name)
                          .set('target', @target)
                          .set('username', @username)
                          .set('password', @password)
                          .set('type', 'shell')

    # ssh connector call to create required machine
    # apply_command_response = connector_call.set("command",@apply_command).timeout(120000).sync

    apply_command_exitcode = 0
    apply_command_message = 'success'

    if apply_command_exitcode == 0
        apply_command_response_body = ` #{@apply_command} `
        @log.info("Success in executing #{@ssh_connector_name} connector, where exitcode :: #{apply_command_exitcode} |
                                                              message :: #{apply_command_message}")
        if apply_command_response_body.include? 'no such file or directory'
            error_body = "Job-id : #{@input.jobid} | Error while provisiong machine : Instance type not supported"
            @output.set('exitcode', 1)
            @output.set('error', error_body)
            @log.error(apply_command_response_body)
        elsif apply_command_response_body.include? 'Error loading config:'
            error_body = "Job-id : #{@input.jobid} | Error while provisiong machine : #{apply_command_response_body}"
            @output.set('exitcode', 1)
            @output.set('error', error_body)
            @log.error(apply_command_response_body)
        elsif apply_command_response_body.include? 'No Terraform configuration files found'
            error_body = "Job-id : #{@input.jobid} | Error while provisiong machine : No configuration found for this instance type"
            @output.set('exitcode', 1)
            @output.set('error', error_body)
            @log.error(apply_command_response_body)
        elsif apply_command_response_body.include? 'Apply complete!'
            # Successfully created machine
            @log.info('Apply command executed successfully..')
            @log.info('Getting state information..')
            # get_state_command_response = connector_call.set("command",@get_state_command).set("type","exec").timeout(60000).sync

            get_state_command_exitcode = 0
            get_state_command_message = 'success'

            if get_state_command_exitcode == 0
                get_state_command_response_body = ` #{@get_state_command} `
                state_result = @util.json(get_state_command_response_body)
                state_module = state_result.get('modules')[0]

                @output.set('exitcode', 0)
                @log.debug('Successfully get state of machine..')
                if @provider.include? 'aws'
                    @output.setraw('state', get_aws_machine_state(state_module).to_s)
                elsif @provider.include? 'google'
                    @output.setraw('state', get_google_machine_state(state_module).to_s)
                end
            else
                @output.set('exitcode', 1)
                error_message = "Instance created successfully but error occured while getting state : #{get_state_command_message}"
                @log.error(error_message)
                @output.set('error', get_state_command_message)
            end
        else
            @output.set('exitcode', 1)
            if apply_command_response_body.include? 'Error applying plan:'
                apply_command_response_body += 'END'
                start_string = 'Error applying plan:'
                end_string = 'END'
                error_string = apply_command_response_body[/#{start_string}(.*?)#{end_string}/m, 1]
                @output.set('error', error_string)
            else
                error_body = "Job-id : #{@input.jobid} | Error while provisiong machine : Internal error occured, please try again.."
                @log.error(apply_command_response_body)
                @output.set('error', error_body)
            end
        end
    else
        @log.error("Failure in executing #{@ssh_connector_name} connector where, exitcode : #{apply_command_exitcode} |
        message : #{apply_command_message}")
        error_body = "Job-id : #{@input.jobid} | Error while provisiong machine : Internal error occured, please try again.."
        @output.set('exitcode', 1)
        @output.set('error', error_body)
    end
rescue Exception => e
    @log.error(e.message)
    @output.set('exit-code', 1).set('message', e.message)
end
@log.trace('Finished execution of jira-terraform:terraform_apply.rb flintbit..')
