@log.trace('Started execution of jira-terraform:add_comment.rb flntbit..')
begin
    # Flintbit input parameters
    @body = @input.get('body')                 # Comment body on jira issue ticket
    @issue_id = @input.get('issue-id')         # Issue ID of jira
    @action = 'add-comment'                    # Action of jira connector
    # Flintbit global configuration of jira
    @connector_name = @config.global('jira.jira_connector')
    @type = @config.global('jira.type')
    @value = @config.global('jira.value')
    @use_proxy = @config.global('jira.use_proxy')
    @proxy = @config.global('jira.proxy')

    response = @call.connector(@connector_name)
                    .set('action', @action)
                    .set('comment', @body)
                    .set('type', @type)
                    .set('value', @value)
                    .set('issue-id', @issue_id)
                    .set('use-proxy', @use_proxy)
                    .set('proxy', @proxy)
                    .timeout(30_000)
                    .sync

    # Jira Connector Response Parameters
    response_result = response.get('body')    # Response Body

    # Jira Connector Response Meta Parameters
    response_exitcode = response.exitcode     # Exit status code
    response_message = response.message       # Execution status message

    if response_exitcode == 0
        @log.info("Successfully added comment over an service request #{@issue_id}")
        @log.info("Success in executing #{@connector_name} connector, where exitcode : " + response_exitcode.to_s + ' | message : ' + response_message)
        @output.set('result', response_result.to_s)
    else
        @log.error("Failure in executing  #{@connector_name} connector, where exitcode : " + response_exitcode.to_s + ' | message : ' + response_message)
        @output.set('error', response_message)
    end
rescue Exception => e
    @log.error(e.message)
    @output.set('exit-code', 1).set('message', e.message)
end
@log.trace('Finished execution of jira-terraform:add_comment.rb flntbit..')
