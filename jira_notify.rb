@log.trace('Started execution of jira-terraform:jira_notify.rb flntbit..')
begin
    @body = @input.get('body')                  # Comment body
    @issue_id = @input.get('issue-id')          # Id of issue in Jira
    @subject = @input.get('subject')            # Notification subject
    @action = 'notify'                          # Action of jira

    @connector_name = @config.global('jira.jira_connector')
    @type = @config.global('jira.type')
    @value = @config.global('jira.value')
    @use_proxy = @config.global('jira.use_proxy')
    @proxy = @config.global('jira.proxy')
    @reporter = true
    @watcher = false
    @assignee = false
    @voters = false

    @log.debug("Notifying user with below details, Body : #{@body}, Subject : #{@subject}, ID : #{@issue_id}")
    # Calling connector to notify user through email
    response = @call.connector(@connector_name)
                    .set('action', @action)
                    .set('body', @body)
                    .set('subject', @subject)
                    .set('issue-id', @issue_id)
                    .set('use-proxy', @use_proxy)
                    .set('proxy', @proxy)
                    .set('reporter', @reporter)
                    .set('assignee', @assignee)
                    .set('watcher', @watcher)
                    .set('voters', @voters)
                    .timeout(30000)
                    .sync

    # Jira Connector Response Meta Parameters
    response_exitcode = response.exitcode
    response_message = response.message

    if response_exitcode == 0
        @log.info("Successfully notify user regarding service request : #{@issue_id}")
    else
        @log.error("Failure in executing  #{@connector_name} connector, where exitcode : " + response_exitcode.to_s + ' | message : ' + response_message)
        @log.error("Failure in notifying user about issue : #{@issue_id}")
    end
rescue Exception => e
    @log.error(e.message)
    @output.set('exit-code', 1).set('message', e.message)
end
@log.trace('Started execution of jira-terraform:jira_notify.rb flntbit..')
