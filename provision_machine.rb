@log.trace('Started execution of jira-terraform:provision_machine.rb flintbit..')
begin
    # Getting required parameters from input
    @issue_id = @input.path('$.issue.id')
    @provider = @input.path('$.issue.fields.customfield_10034.value')
    @instance_type = @input.path('$.issue.fields.customfield_10032.value')
    @os = @input.path('$.issue.fields.customfield_10033.value')

    @output.exit(1, 'Please provide issue-id') if @issue_id.nil?

    if @provider.nil?
        error_comment = "Job-id : #{@input.jobid} | Error while provisiong machine : Please provide provider name"
        @call.bit('jira-terraform:add_comment.rb').set('body', error_comment).set('issue-id', @issue_id).async
        @output.exit(1, 'Please provide provider name')
    end
    if @instance_type.nil?
        error_comment = "Job-id : #{@input.jobid} | Error while provisiong machine : Please provide instance type e.q bronze, silver, gold"
        @call.bit('jira-terraform:add_comment.rb').set('body', error_comment).set('issue-id', @issue_id).async
        @output.exit(1, 'Please provide instance type e.q bronze, silver, gold')
    end
    if @os.nil?
        error_comment = "Job-id : #{@input.jobid} | Error while provisiong machine : Please provide OS"
        @call.bit('jira-terraform:add_comment.rb').set('body', error_comment).set('issue-id', @issue_id).async
        @output.exit(1, 'Please provide OS')
    end

    # Adding comment over an issue from Jira
    comment_body = "Provisioning an virtual machine based on provided credentials using Flint, Job-id : #{@input.jobid}"
    @call.bit('jira-terraform:add_comment.rb').set('body', comment_body).set('issue-id', @issue_id).async

    flintbit_response = @call.bit('jira-terraform:terraform_apply.rb')
                             .set('id', @issue_id)
                             .set('provider', @provider)
                             .set('instance_type', @instance_type)
                             .set('os', @os)
                             .timeout(180_000)
                             .sync
    flintbit_exitcode = flintbit_response.exitcode
    flintbit_message = flintbit_response.message
    @comment_body = ''

    if flintbit_exitcode == 0
        if flintbit_response.get('exitcode') == 0
            flintbit_state = flintbit_response.get('state')
            @state_of_machine = flintbit_response.get('state')
            @log.info('Successfully created machine with below details')
            @log.info("State of machine : #{@state_of_machine}")
            @provider.downcase!
            if @provider.include? 'aws'
                id = @state_of_machine['id']
                availability_zone = @state_of_machine['availability_zone']
                instance_type = @state_of_machine['instance_type']
                private_ip = @state_of_machine['private_ip']
                public_ip = @state_of_machine['public_ip']
                @comment_body = "Created an instance with following details, \nInstance ID : #{id} \n Availability zone : #{availability_zone} \n Instance type : #{@instance_type} \n Private IP : #{private_ip} \n Public IP : #{public_ip}"
                @email_body = "Your new virtual machine is all set to go!, You can access it using following information. <br/><br/> VM ID : #{id} <br/> Availability zone : #{availability_zone} <br/> Instance type : #{@instance_type} <br/> Private IP : #{private_ip} <br/> Public IP : #{public_ip} <br/><br/><br/>Regards,<br/>Flint support"
            else
                id = @state_of_machine['id']
                zone = @state_of_machine['zone']
                public_ip = @state_of_machine['public_ip']
                @comment_body = "Created an instance with following details, \nInstance ID : #{id} \n Zone : #{zone} \n Public IP : #{public_ip}"
                @email_body = "Your new virtual machine is all set to go!, You can access it using following information. <br/><br/> VM ID : #{id} <br/> Zone : #{zone} <br/> Public IP : #{public_ip}<br/><br/><br/>Regards,<br/>Flint support"
            end
            # Notifying user about status of an service request
            @email_subject = 'Your VM has been created'
            @call.bit('jira-terraform:jira_notify.rb').set('subject', @email_subject).set('body', @email_body).set('issue-id', @issue_id).async

            # Change status of an service request from Jira
            @call.bit('jira-terraform:change_status.rb').set('comment', 'Instance has been created').set('issue-id', @issue_id).async
        else
            error_message = flintbit_response.get('error')
            @comment_body = error_message
            @log.error(error_message)
        end
    else
        error_message = "Error while executing flintbit to provision an virtual machine : #{flintbit_message}"
        @comment_body = error_message
        @log.error(error_message)
    end
    # Adding comment over an issue from Jira
    @call.bit('jira-terraform:add_comment.rb').set('body', @comment_body).set('issue-id', @issue_id).async
rescue Exception => e
    @log.error(e.message)
    @output.set('exit-code', 1).set('message', e.message)
end
@log.trace('Finished execution of jira-terraform:provision_machine.rb flintbit..')
